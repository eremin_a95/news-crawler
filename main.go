package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"runtime"
	"strings"

	"github.com/hu17889/go_spider/core/spider"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

// RunSpider creates new spider and runs it
func RunSpider(tp *TopicProcesser) {
	spider.NewSpider(tp, tp.BaseURL.String()+" task").
		SetSleepTime("fixed", 500, 0).
		AddUrl(tp.BaseURL.String(), "html").
		SetThreadnum(uint(runtime.NumCPU())).
		Run()
}

// Usage prints usage info
func Usage() {
	fmt.Println("Usage: tochka-test config.yaml")
	fmt.Println("  config.yaml - YAML-file which contains parsing rules")
	fmt.Println("")
	fmt.Println("config.yaml contains list of dictionaries:")
	fmt.Println("  url: <url.to.news-site>    - URL to news website")
	fmt.Println("  topic_container_selectors: - list of selectors for elements")
	fmt.Println("    - <\"selector1\">          which contain topic")
	fmt.Println("    - <\"selector2\">")
	fmt.Println("      ...")
	fmt.Println("  topic_title_selectors:     - list of selectors for elements")
	fmt.Println("    - <\"selector1\">          which contain title of topic")
	fmt.Println("    - <\"selector2\">          as text")
	fmt.Println("      ...")
	fmt.Println("  topic_body_selectors:      - list of selectors for elements")
	fmt.Println("    - <\"selector1\">          which contain body of topic")
	fmt.Println("    - <\"selector2\">          as text")
	fmt.Println("      ...")
	fmt.Println("  topic_published_selectors: - list of selectors for elements")
	fmt.Println("    - <\"selector1\">          which contain info about")
	fmt.Println("    - <\"selector2\">          publishing of topic as text")
	fmt.Println("      ...")
	fmt.Println("  link_selectors:            - list of selectors for elements")
	fmt.Println("    - <\"selector1\">          which contain links to topics")
	fmt.Println("    - <\"selector2\">")
	fmt.Println("      ...")
}

// ShellHelp prints help for user about interactive shell
func ShellHelp() {
	fmt.Println("Print \"q\", \"quit\" or \"exit\" to quit this application")
	fmt.Println("Print \"h\", \"help\" or empty line to print this help")
	fmt.Println("Print \"list <search_token>\" to list titles of topics")
	fmt.Println("    which contains <search_token> substring")
	fmt.Println("  If there is only one topic returns, its body and info will")
	fmt.Println("    be printed too")
	fmt.Println("Print \"list *\" to list all titles")
	fmt.Println("You can use '*' as any number wildcard")
	fmt.Println("  and '?' as a single wildcard")
}

func main() {
	if len(os.Args) != 2 {
		Usage()
		return
	}

	db, err := gorm.Open("sqlite3", "news.db")
	if err != nil {
		log.Panicf("Failed to connect database: %s", err)
	}
	defer db.Close()

	logfile, err := os.OpenFile(os.Args[0]+".log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		log.Panicf("Failed to open log file %s: %v", os.Args[0]+".log", err)
	}
	defer logfile.Close()
	os.Stderr = logfile
	db.LogMode(false).AutoMigrate(&Topic{})

	conf, err := os.Open(os.Args[1])
	if err != nil {
		log.Panicf("Failed to open config file %s: %v", os.Args[1], err)
	}
	tps, err := ParseConfig(conf)
	if err != nil {
		log.Panicf("Failed to parse config file %s: %v", os.Args[1], err)
	}

	for _, tp := range tps {
		tp.db = db
		go RunSpider(tp)
	}

	var str string
	in := bufio.NewReader(os.Stdin)
	topics := make([]Topic, 0, 8)
	for _, err := fmt.Print("> "); err == nil; _, err = fmt.Print("> ") {
		str, _ = in.ReadString('\n')
		str = strings.TrimSpace(str)
		switch str {
		case "q", "quit", "exit":
			return
		case "", "h", "help":
			ShellHelp()
		default:
			args := strings.Split(str, "list ")
			if len(args) != 2 {
				ShellHelp()
				continue
			}
			str = "%" + strings.ReplaceAll(strings.ReplaceAll(args[1], "?", "_"), "*", "%") + "%"
			db.Where("title LIKE ?", str).Find(&topics)
			if len(topics) == 1 {
				fmt.Printf("%s\n\n%s\n%s\n%s\n", topics[0].Title, topics[0].Body, topics[0].Published, topics[0].URL)
				continue
			}
			for _, topic := range topics {
				fmt.Print(topic.Title, "\n\n")
			}
			fmt.Printf("Total: %v topics\n", len(topics))
		}

	}
}
