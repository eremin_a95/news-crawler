package main

import (
	"io"
	"net/url"

	"gopkg.in/yaml.v2"
)

// YamlTP is struct to store yaml values
type YamlTP struct {
	URL                     string   `yaml:"url"`
	TopicContainerSelectors []string `yaml:"topic_container_selectors"`
	TopicTitleSelectors     []string `yaml:"topic_title_selectors"`
	TopicPublishedSelectors []string `yaml:"topic_published_selectors"`
	TopicBodySelectors      []string `yaml:"topic_body_selectors"`
	LinkSelectors           []string `yaml:"link_selectors"`
}

// ParseConfig reads YAML config from reader
// and parse it to slice of pointers to TopicProcesser
func ParseConfig(reader io.Reader) ([]*TopicProcesser, error) {
	buf := make([]byte, 4096)
	n, err := reader.Read(buf)
	if err != nil && err != io.EOF {
		return nil, err
	}
	ytps := make([]YamlTP, 0, 4)
	yaml.Unmarshal(buf[:n], &ytps)
	tps := make([]*TopicProcesser, len(ytps))
	for i := range tps {
		URL, err := url.Parse(ytps[i].URL)
		if err != nil {
			return nil, err
		}
		tps[i] = &TopicProcesser{
			BaseURL:                 URL,
			TopicContainerSelectors: ytps[i].TopicContainerSelectors,
			TopicTitleSelectors:     ytps[i].TopicTitleSelectors,
			TopicBodySelectors:      ytps[i].TopicBodySelectors,
			TopicPublishedSelectors: ytps[i].TopicPublishedSelectors,
			LinkSelectors:           ytps[i].LinkSelectors,
		}
	}
	return tps, nil
}
