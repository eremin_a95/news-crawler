package main

import (
	"bytes"
	"net/url"
	"testing"
)

const YamlConfigExample = `- url: "https://russian.rt.com/news"
  topic_container_selectors:
    - ".article"
  topic_title_selectors:
    - ".article__heading"
  topic_body_selectors:
    - ".article__summary"
    - ".article__text"
  topic_published_selectors:
    - ".article__date time"
  link_selectors:
    - ".card__heading_all-news a"
- url: "https://ria.ru"
  topic_container_selectors:
    - ".layout-article"
    - ".b-longread"
  topic_title_selectors:
    - ".article__title"
    - ".b-longread__widget-text.m-input_title_article"
    - ".b-longread__widget-text.m-input_subtitle_article"
  topic_body_selectors:
    - ".article__text"
    - ".b-longread__row .b-longread__widget-text:not(.m-input_title_article):not(.m-input_subtitle_article)"
  topic_published_selectors:
    - ".article__info-date a"
  link_selectors:
    - ".cell-list__item-link"
    - ".cell-main-photo__link"`

func TestParseConfig(t *testing.T) {
	ria := NewTopicProcesser(nil)
	ria.BaseURL, _ = url.Parse("https://ria.ru")
	ria.TopicContainerSelectors = []string{".layout-article", ".b-longread"}
	ria.TopicTitleSelectors = []string{".article__title", ".b-longread__widget-text.m-input_title_article", ".b-longread__widget-text.m-input_subtitle_article"}
	ria.TopicBodySelectors = []string{".article__text", ".b-longread__row .b-longread__widget-text:not(.m-input_title_article):not(.m-input_subtitle_article)"}
	ria.TopicPublishedSelectors = []string{".article__info-date a"}
	ria.LinkSelectors = []string{".cell-list__item-link", ".cell-main-photo__link"}

	rt := NewTopicProcesser(nil)
	rt.BaseURL, _ = url.Parse("https://russian.rt.com/news")
	rt.TopicContainerSelectors = []string{".article"}
	rt.TopicTitleSelectors = []string{".article__heading"}
	rt.TopicBodySelectors = []string{".article__summary", ".article__text"}
	rt.TopicPublishedSelectors = []string{".article__date time"}
	rt.LinkSelectors = []string{".card__heading_all-news a"}

	gauge := make([]*TopicProcesser, 0, 2)
	gauge = append(gauge, rt, ria)

	tps, err := ParseConfig(bytes.NewReader([]byte(YamlConfigExample)))
	if err != nil {
		t.Errorf("Unexpected error: %v\n", err)
		t.FailNow()
	}
	if len(tps) != len(gauge) {
		t.Errorf("For len(tps) expected %v returned %v\n", len(gauge), len(tps))
	}
	for i := range tps {
		if tps[i].BaseURL.String() != gauge[i].BaseURL.String() {
			t.Errorf("For tps[i].BaseURL.String() expected %v returned %v\n", gauge[i].BaseURL.String(), tps[i].BaseURL.String())
		}
		if len(tps[i].TopicContainerSelectors) != len(gauge[i].TopicContainerSelectors) {
			t.Errorf("For len(tps[%v].TopicContainerSelectors) expected %v returned %v\n", i, len(gauge[i].TopicContainerSelectors), len(tps[i].TopicContainerSelectors))
		}
		for j := range tps[i].TopicContainerSelectors {
			if tps[i].TopicContainerSelectors[j] != gauge[i].TopicContainerSelectors[j] {
				t.Errorf("For tps[%v].TopicContainerSelectors[%v] expected %v returned %v\n", i, j, gauge[i].TopicContainerSelectors[j], tps[i].TopicContainerSelectors[j])
			}
		}
		if len(tps[i].TopicTitleSelectors) != len(gauge[i].TopicTitleSelectors) {
			t.Errorf("For len(tps[%v].TopicTitleSelectors) expected %v returned %v\n", i, len(gauge[i].TopicTitleSelectors), len(tps[i].TopicTitleSelectors))
		}
		for j := range tps[i].TopicTitleSelectors {
			if tps[i].TopicTitleSelectors[j] != gauge[i].TopicTitleSelectors[j] {
				t.Errorf("For tps[%v].TopicTitleSelectors[%v] expected %v returned %v\n", i, j, gauge[i].TopicTitleSelectors[j], tps[i].TopicTitleSelectors[j])
			}
		}
		if len(tps[i].TopicBodySelectors) != len(gauge[i].TopicBodySelectors) {
			t.Errorf("For len(tps[%v].TopicBodySelectors) expected %v returned %v\n", i, len(gauge[i].TopicBodySelectors), len(tps[i].TopicBodySelectors))
		}
		for j := range tps[i].TopicBodySelectors {
			if tps[i].TopicBodySelectors[j] != gauge[i].TopicBodySelectors[j] {
				t.Errorf("For tps[%v].TopicBodySelectors[%v] expected %v returned %v\n", i, j, gauge[i].TopicBodySelectors[j], tps[i].TopicBodySelectors[j])
			}
		}
		if len(tps[i].TopicPublishedSelectors) != len(gauge[i].TopicPublishedSelectors) {
			t.Errorf("For len(tps[%v].TopicPublishedSelectors) expected %v returned %v\n", i, len(gauge[i].TopicPublishedSelectors), len(tps[i].TopicPublishedSelectors))
		}
		for j := range tps[i].TopicPublishedSelectors {
			if tps[i].TopicPublishedSelectors[j] != gauge[i].TopicPublishedSelectors[j] {
				t.Errorf("For tps[%v].TopicPublishedSelectors[%v] expected %v returned %v\n", i, j, gauge[i].TopicPublishedSelectors[j], tps[i].TopicPublishedSelectors[j])
			}
		}
		if len(tps[i].LinkSelectors) != len(gauge[i].LinkSelectors) {
			t.Errorf("For len(tps[%v].LinkSelectors) expected %v returned %v\n", i, len(gauge[i].LinkSelectors), len(tps[i].LinkSelectors))
		}
		for j := range tps[i].LinkSelectors {
			if tps[i].LinkSelectors[j] != gauge[i].LinkSelectors[j] {
				t.Errorf("For tps[%v].LinkSelectors[%v] expected %v returned %v\n", i, j, gauge[i].LinkSelectors[j], tps[i].LinkSelectors[j])
			}
		}
	}
}
