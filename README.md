# news-crawler

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/eremin_a95/news-crawler)](https://goreportcard.com/report/gitlab.com/eremin_a95/news-crawler)

## Install
    git clone https://gitlab.com/eremin_a95/news-crawler
    cd news-crawler
    go build

## Usage

### Run

Run program usage example:

    news-crawler config.yaml

Where `config.yaml` - YAML-file which contains parsing rules

`config.yaml` contains list of dictionaries:

- URL to news website:
```yaml
url: <"url.to.news-site">
```

- list of selectors for elements  which contain topic:
```yaml
topic_container_selectors:
    - <selector1>
    - <selector2>
```

- list of selectors for elements which contain title of topic as text:
```yaml
topic_title_selectors:
    - <selector1>
    - <selector2>
```

- list of selectors for elements which contain body of topic as text:
```yaml
topic_body_selectors:
    - <selector1>
    - <selector2>
```

- list of selectors for elements which contain info about publishing of topic as text:
```yaml
topic_published_selectors:
    - <selector1>
    - <selector2>
```

- list of selectors for elements which contain to topics:
```yaml
link_selectors:
    - <selector1>
    - <selector2>
```


### Shell

While crawler is executing you can watch crawled topics.

Print `h`, `help` or empty string (just press `Enter`) to watch shell help message.

Print `q`, `quit` or `exit` to stop the programm execution.

Print `list <search_token>` to to list titles of topics which contains <search_token> substring.

If there is only one topic returns, its body, info and URL will be printed too.

You can use `*` as any number wildcard and `?` as single character wildcard.

Thus, you can print `list *` to print all titles.

## Parsing rules examples

 Next parsing rules are intended for [ria.ru](https://ria.ru) and  [russian.rt.com](https://russian.rt.com).

```yaml
- url: "https://russian.rt.com"
  topic_container_selectors:
    - ".article:not(.article_error-pages)"
    - ".content-wrap .left-column.page"
  topic_title_selectors:
    - ".article__heading"
    - ".content-wrap .left-column h1"
  topic_body_selectors:
    - ".article__summary"
    - ".article__text p"
    - ".article-intro"
    - " .article-body"
  topic_published_selectors:
    - ".article__date time"
  link_selectors:
    - ".card__heading_all-news a"
    - ".card__heading .link"
    - ".nav__link.link"
    - ".content-wrap .left-column .link-overlay"
- url: "https://ria.ru"
  topic_container_selectors:
    - ".layout-article"
    - ".b-longread"
  topic_title_selectors:
    - ".article__title"
    - ".b-longread__widget-text.m-input_title_article"
    - ".b-longread__widget-text.m-input_subtitle_article"
  topic_body_selectors:
    - ".article__text"
    - ".b-longread__row .b-longread__widget-text:not(.m-input_title_article):not(.m-input_subtitle_article)"
  topic_published_selectors:
    - ".article__info-date a"
  link_selectors:
    - ".cell-list__item-link"
    - ".cell-main-photo__link"
    - ".cell-extension__item-title"
    - ".list-item__content .list-item__title"
```