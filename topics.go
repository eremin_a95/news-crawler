package main

import (
	"fmt"
	"log"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/hu17889/go_spider/core/common/page"
	"github.com/jinzhu/gorm"
)

// Topic is struct to handle with DB topic rows
type Topic struct {
	gorm.Model
	Title, Body string
	Published   string
	Parsed      time.Time `gorm:"default:CURRENT_TIMESTAMP"`
	URL         string    `gorm:"unique_index;not null"`
}

// TopicProcesser it type to process crawling for topics
type TopicProcesser struct {
	db                      *gorm.DB
	BaseURL                 *url.URL
	TopicContainerSelectors []string
	TopicTitleSelectors     []string
	TopicPublishedSelectors []string
	TopicBodySelectors      []string
	LinkSelectors           []string
}

// NewTopicProcesser returns pointer to new instance of TopicProcesser
func NewTopicProcesser(db *gorm.DB) *TopicProcesser {
	return &TopicProcesser{db: db}
}

// Process implements PageProcesser interface
func (tp *TopicProcesser) Process(p *page.Page) {
	if !p.IsSucc() {
		log.Println(p.Errormsg())
		return
	}

	var urls []string
	query := p.GetHtmlParser()
	for _, containerSelector := range tp.TopicContainerSelectors {
		query.Find(containerSelector).Each(func(i int, s *goquery.Selection) {
			var topic Topic
			for _, titleSelector := range tp.TopicTitleSelectors {
				s.Find(titleSelector).Each(func(i int, s *goquery.Selection) {
					topic.Title += strings.TrimSpace(s.Text()) + "\r\n"
				})
			}
			for _, publishedSelector := range tp.TopicPublishedSelectors {
				s.Find(publishedSelector).Each(func(i int, s *goquery.Selection) {
					//topic.Published, _ = time.Parse("15:04 02.01.2006", s.Text())
					topic.Published += strings.TrimSpace(s.Text()) + "\r\n"
				})
			}
			for _, bodySelector := range tp.TopicBodySelectors {
				s.Find(bodySelector).Each(func(i int, s *goquery.Selection) {
					topic.Body += strings.TrimSpace(s.Text()) + "\r\n"
				})
			}
			topic.Parsed = time.Now()
			topic.URL = p.GetRequest().GetUrl()
			tp.db.Create(&topic)
		})
	}
	for _, selector := range tp.LinkSelectors {
		query.Find(selector).Each(func(i int, s *goquery.Selection) {
			var topic Topic
			if url, ok := s.Attr("href"); ok {
				if strings.HasPrefix(url, "/") && !strings.HasPrefix(url, "www.") {
					url = tp.BaseURL.Scheme + "://" + tp.BaseURL.Hostname() + url
				}
				if tp.db.Find(&topic, "url = ?", url).RecordNotFound() {
					urls = append(urls, url)
				}
			}
		})
	}

	p.AddTargetRequests(urls, "html")
}

// Finish implements PageProcesser interface
func (tp *TopicProcesser) Finish() {
	time.Sleep(time.Second * 5)
	go RunSpider(tp)
	fmt.Fprintf(os.Stderr, "Restart crawler for %s\r\n", tp.BaseURL.String())
}
