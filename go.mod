module gitlab.com/eremin_a95/news-crawler

go 1.12

require (
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/bitly/go-simplejson v0.5.0 // indirect
	github.com/hu17889/go_spider v0.0.0-20150809033053-85ede20bf88b
	github.com/jinzhu/gorm v1.9.11
	gopkg.in/yaml.v2 v2.2.4
)
